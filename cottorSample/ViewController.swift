//
//  ViewController.swift
//  cottorSample
//
//  Created by Krishna Sunkara on 29/06/21.
//

import UIKit
import Cotter

struct CotterDetails {
    static let APIKey = "335a5c66-8783-472c-b7e1-125b90c61e38"
    static let SecretKey = "EBsaMTaVfWFGWsCZFkee"
    static let cotterUrl = "https://www.cotter.app/api/v0"
    static let userId = "krishna@zuddl.com"
}

class ViewController: UIViewController {
    
    let cotter = Cotter(apiSecretKey: CotterDetails.SecretKey, apiKeyID: CotterDetails.APIKey, cotterURL: CotterDetails.cotterUrl, userID: CotterDetails.userId, configuration: [:])

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        cotter.startPasswordlessLogin(parentView: self, input: "krishna@zuddl.com", identifierField: "email", type: "EMAIL", directLogin: true) { (identity, error) in
            if error == nil {
                print(identity)
                if let accessToken = Cotter.getAccessToken() {
                    print("access token is \(accessToken)")
                }
            } else {
                print(error)
            }
        }
        
    }


}

